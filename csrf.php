<?php
/* USAGE:
// Simplistic
$token = boost()->csrf->generate();
$valid = boost()->csrf->check($token);

// Multiple tokens simultaneously
$token_1 = boost()->csrf->generate('key1');
$token_2 = boost()->csrf->generate('key2');
$token_3 = boost()->csrf->generate('key3');
$not_valid = boost()->csrf->check($token_2, 'key3');
$valid = boost()->csrf->check($token_2, 'key2');
*/

namespace Boost;

boost()->add_callable('csrf', 'Boost\CSRF');

Class CSRF extends Library {

	// Adapted from https://github.com/BKcore/NoCSRF

	private $strict_mode = true;

	// Determines if the incoming value and key are valid
	// Passing in a timestamp will make sure it isn't too old
	function check($incoming_value = null, $key = null, $timespan_in_seconds = null) {
		$session_key = $this->get_session_name_from_key($key);
		$session_value = boost()->session->get($session_key);

		// Check for strict info
		if ($this->strict_mode && $this->get_strict_info() != substr(base64_decode($session_value), 42, 40)) {
			return false;
		}

		// Check for token value
		if ($session_value != $incoming_value) {
			return false;
		}

		// Check for timestamp expiration
		if ($timespan_in_seconds != null && is_int($timespan_in_seconds) && intval(substr(base64_decode($session_value), 32, 10)) + $timespan_in_seconds < time()) {
			return false;
		}

		return true;
	}

	// Enable or disable strict mode
	function strict_mode($enabled = true) {
		$this->strict_mode = $enabled;
		return $this;
	}

	// Create a new token
	function generate($key = null, $store = true) {
		$extra = $this->strict_mode ? $this->get_strict_info() : '';
		$token = base64_encode($this->generate_random_string().time().$extra);
		if ($store) {
			$this->store($key, $token);
		}
		return $token;
	}

	// Store a token
	function store($key = null, $token = null) {
		boost()->session->set($this->get_session_name_from_key($key), $token);
	}

	private function generate_random_string() {
		$length = 32;
		$seed = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijqlmnopqrtsuvwxyz0123456789';
		$max = strlen($seed)-1;

		$string = '';
		for ($i = 0; $i < $length; ++$i) {
			$string .= $seed{intval(mt_rand(0.0, $max))};
		}

		return $string;
	}

	// Converts a key name into a session key name for consistency
	private function get_session_name_from_key($key = null) {
		return 'csrf_'.md5($this->get_strict_info().$key);
	}

	// Generates a string specific to the active user's machine
	private function get_strict_info() {
		return sha1($_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT']);
	}

}